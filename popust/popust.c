/* vim:fdm=marker ts=4 et ai
 * {{{
 *
 * Copyright (c) 2008 by Jochen Roessner <jochen@lugrot.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * For more information on the GPL, please go to:
 * http://www.gnu.org/copyleft/gpl.html
 }}} */

#include <stdint.h>
#include "../config.h"
#include "popust.h"
#include <avr/io.h>
#include <avr/interrupt.h>



/* [0]: timer, [1]: flag */
static uint16_t pumpe_tmp[4][2];
enum {
  PUMPE_EIN,
  PUMPE_AUS,
};

void
popust_init (void)
{
  POPUST_DDR |= ((1 << POPUST_PINS) - 1) << POPUST_OFFSET; 
  POPUST_LEDS_DDR |= ((1 << POPUST_LEDS_PINS) - 1) << POPUST_LEDS_OFFSET; 
  DDR_CONFIG_OUT(POPUST_STATUSLED);
#ifdef ZEROCROSSING_SUPPORT
  MCUCR |= POPUST_INT_CFG_SET; //_BV(ISC11) | _BV(ISC10);  
  GICR |= _BV(POPUST_INT_PIN);
#endif
}

static uint16_t
expand_value(uint8_t wert)
{
  return (wert & 3) + (wert * ((wert >> 2)));
}


void
popust_pumpe_reset(uint8_t pumpenr) 
{
  if (pumpe[pumpenr][0] != 0xff){
    if (pumpe_tmp[pumpenr][1] == PUMPE_EIN){
      if (pumpe_tmp[pumpenr][0] >= expand_value(pumpe[pumpenr][1]))
        pumpe_tmp[pumpenr][0] = 1;
    }
    else{
      if (pumpe_tmp[pumpenr][0] >= expand_value(pumpe[pumpenr][0]))
        pumpe_tmp[pumpenr][0] = expand_value(pumpe[pumpenr][0]);
    }
  }
}

void
popust_timer (void)
{
  uint8_t pumpenr;
  for(pumpenr = 0; pumpenr < 4; pumpenr ++){
    if (pumpe[pumpenr][0] != 0xff){
      pumpe_tmp[pumpenr][0]++;
      if (pumpe_tmp[pumpenr][1] == PUMPE_EIN && pumpe_tmp[pumpenr][0] >= expand_value(pumpe[pumpenr][1])) {
         if (!(pumpe[pumpenr][1] && !pumpe[pumpenr][0]))
           PORTC &= ~_BV(pumpenr);
         pumpe_tmp[pumpenr][0] = 0;
         pumpe_tmp[pumpenr][1] = PUMPE_AUS;
      } else if (pumpe_tmp[pumpenr][1] == PUMPE_AUS && pumpe_tmp[pumpenr][0] >= expand_value(pumpe[pumpenr][0])) {
         if (pumpe[pumpenr][1] == 0)
           PORTC &= ~_BV(pumpenr);
         else
           PORTC |= _BV(pumpenr);
         pumpe_tmp[pumpenr][0] = 0;
         pumpe_tmp[pumpenr][1] = PUMPE_EIN;
      }
    }
    else{
      if(pumpenr > 0 && pumpe[pumpenr][1] <= pumpe[pumpenr-1][0] && 
        pumpe_tmp[pumpenr-1][1] == PUMPE_AUS &&
        pumpe_tmp[pumpenr-1][0] > (expand_value(pumpe[pumpenr-1][0]) - expand_value(pumpe[pumpenr][1]))
	){
        PORTC |= _BV(pumpenr);
      }
      else{
        PORTC &= ~_BV(pumpenr);
      }
    }
  }
}

