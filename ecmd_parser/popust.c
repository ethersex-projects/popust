/* vim:fdm=marker ts=4 et ai
 * {{{
 *
 * Copyright (c) 2008 by Jochen Roessner <jochen@lugrot.de>
 * Copyright (c) 2008 by Christian Dietrich <stettberger@dokucode.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * For more information on the GPL, please go to:
 * http://www.gnu.org/copyleft/gpl.html
 }}} */

#include <string.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>
#include <avr/interrupt.h>

#include "../config.h"
#include "../debug.h"
#include "../uip/uip.h"
#include "../uip/uip_arp.h"
#include "../eeprom.h"
#include "../popust/popust.h"
#include "../bit-macros.h"
#include "../portio.h"
#include "ecmd.h"

#define NIBBLE_TO_HEX(a) ((a) < 10 ? (a) + '0' : ((a) - 10 + 'A')) 

#ifdef POPUST_SUPPORT
uint8_t pumpe[4][2];

int16_t parse_cmd_pumpe(char *cmd, char *output, uint16_t len)
/* {{{ */ {
  uint8_t pumpenr = 0;
  uint8_t pumpeein;
  uint8_t pumpeaus;
  uint8_t ret = 0;
  if (cmd[0]) {
    if ( (cmd[0] - '0') < 4) {
      pumpenr = cmd[0] - '0';
    } else 
      return -1;
  }
  cmd += 1;
  while (*cmd == ' ')
    cmd ++;
  if(*cmd != 0) {
    char *p = strchr(cmd, ' ');
    if (! p) return -1;
    pumpeein = strtol(cmd, NULL, 16);
    if(pumpeein > 0)
      POPUST_LEDS_PORT |= (1 << (pumpenr + POPUST_LEDS_OFFSET));
    else
      POPUST_LEDS_PORT &= ~(1 << (pumpenr + POPUST_LEDS_OFFSET));
    while (*p == ' ')
      p ++;
    if(! *p) return -1;
    pumpeaus = strtol(p, NULL, 16);
    pumpe[pumpenr][0] = pumpeaus;
    pumpe[pumpenr][1] = pumpeein;
    popust_pumpe_reset(pumpenr);
  }

  output[0] = pumpenr + '0';
  output[1] = ':';
  output[2] = ' ';
  output[3] = NIBBLE_TO_HEX((pumpe[pumpenr][1] >> 4) & 0x0F);
  output[4] = NIBBLE_TO_HEX(pumpe[pumpenr][1] & 0x0F);
  output[5] = ' ';
  output[6] = NIBBLE_TO_HEX((pumpe[pumpenr][0] >> 4) & 0x0F);
  output[7] = NIBBLE_TO_HEX(pumpe[pumpenr][0] & 0x0F);
  output[8] = 0;
 
  return 8;
} /* }}} */
#endif
